﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="InsererFilm.aspx.cs" Inherits="_420_PS1_lab2.InsererFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    <title>InsererFilm</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderImage" runat="server">
    <img class="mr-3 imgSize " src="../img/logoTeccart.jpeg" alt="Image Teccart">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderInfo" runat="server">
    <form id="form1" runat="server">
    <blockquote class="blockquote text-center">
        <p class="mb-0">Insertion des film dans la base de donnee</p>
        <footer class="blockquote-footer">Ajout d'une photo </footer>
    </blockquote>
    
    <div>
        
        <div class="row main">
            <div class="col-4 ">
                <blockquote class="blockquote text-center">
                    <p class="mb-0">Insertion film</p>
                    <footer class="blockquote-footer">table film </footer>
                </blockquote>
                
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="idfilm" DataSourceID="SqlDataSource1" DefaultMode="Insert" Height="50px" Width="125px">
            <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <Fields>
                <asp:BoundField DataField="idfilm" HeaderText="Id Film" ReadOnly="True" SortExpression="idfilm" />
                <asp:BoundField DataField="titre" HeaderText="Titre" SortExpression="titre" />
                <asp:BoundField DataField="nomdirecteur" HeaderText="Directeur" SortExpression="nomdirecteur" />
                <asp:BoundField DataField="liens" HeaderText="Hyperlien" SortExpression="liens" />
                <asp:BoundField DataField="photo" HeaderText="Photo" SortExpression="photo" />
                <asp:CommandField ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [film] WHERE [idfilm] = @idfilm" InsertCommand="INSERT INTO [film] ([idfilm], [titre], [nomdirecteur], [liens], [photo]) VALUES (@idfilm, @titre, @nomdirecteur, @liens, @photo)" SelectCommand="SELECT * FROM [film]" UpdateCommand="UPDATE [film] SET [titre] = @titre, [nomdirecteur] = @nomdirecteur, [liens] = @liens, [photo] = @photo WHERE [idfilm] = @idfilm">
            <DeleteParameters>
                <asp:Parameter Name="idfilm" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="idfilm" Type="Int32" />
                <asp:Parameter Name="titre" Type="String" />
                <asp:Parameter Name="nomdirecteur" Type="String" />
                <asp:Parameter Name="liens" Type="String" />
                <asp:Parameter Name="photo" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="titre" Type="String" />
                <asp:Parameter Name="nomdirecteur" Type="String" />
                <asp:Parameter Name="liens" Type="String" />
                <asp:Parameter Name="photo" Type="String" />
                <asp:Parameter Name="idfilm" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
                
                <asp:FileUpload ID="FileUpload1" runat="server" />

            </div>
    
            <div class="col-4 ">
                <blockquote class="blockquote text-center">
                    <p class="mb-0">Insertion categorie</p>
                    <footer class="blockquote-footer">table categorie </footer>
                </blockquote>
 
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [categorie] WHERE [idcategorie] = @idcategorie" InsertCommand="INSERT INTO [categorie] ([idcategorie], [nomcategorie]) VALUES (@idcategorie, @nomcategorie)" SelectCommand="SELECT * FROM [categorie]" UpdateCommand="UPDATE [categorie] SET [nomcategorie] = @nomcategorie WHERE [idcategorie] = @idcategorie">
                    <DeleteParameters>
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                        <asp:Parameter Name="nomcategorie" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="nomcategorie" Type="String" />
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                
                <asp:DetailsView ID="DetailsView2" runat="server" Height="50px" Width="125px" AutoGenerateRows="False" DataKeyNames="idcategorie" DataSourceID="SqlDataSource2" DefaultMode="Insert">
                    <Fields>
                        <asp:BoundField DataField="idcategorie" HeaderText="ID Categorie" ReadOnly="True" SortExpression="idcategorie" />
                        <asp:BoundField DataField="nomcategorie" HeaderText="Nom Categorie" SortExpression="nomcategorie" />
                        <asp:CommandField ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
                


            </div>
            <div class="col-4 ">
                <blockquote class="blockquote text-center">
                    <p class="mb-0">Insertion cinema</p>
                    <footer class="blockquote-footer">table cinema                      
                    </footer>
                </blockquote>
                
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [cinema] WHERE [idfilm] = @idfilm AND [idcategorie] = @idcategorie AND [datepub] = @datepub" InsertCommand="INSERT INTO [cinema] ([idfilm], [idcategorie], [datepub], [cinemaproj]) VALUES (@idfilm, @idcategorie, @datepub, @cinemaproj)" SelectCommand="SELECT * FROM [cinema]" UpdateCommand="UPDATE [cinema] SET [cinemaproj] = @cinemaproj WHERE [idfilm] = @idfilm AND [idcategorie] = @idcategorie AND [datepub] = @datepub">
                    <DeleteParameters>
                        <asp:Parameter Name="idfilm" Type="Int32" />
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                        <asp:Parameter DbType="Date" Name="datepub" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="idfilm" Type="Int32" />
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                        <asp:Parameter DbType="Date" Name="datepub" />
                        <asp:Parameter Name="cinemaproj" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="cinemaproj" Type="Boolean" />
                        <asp:Parameter Name="idfilm" Type="Int32" />
                        <asp:Parameter Name="idcategorie" Type="Int32" />
                        <asp:Parameter DbType="Date" Name="datepub" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                
                <asp:DetailsView ID="DetailsView3" runat="server" Height="50px" Width="125px" AutoGenerateRows="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="idfilm,idcategorie,datepub" DataSourceID="SqlDataSource3" DefaultMode="Insert" GridLines="Horizontal" OnPageIndexChanging="DetailsView3_PageIndexChanging">
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                    <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <Fields>
                        <asp:BoundField DataField="idfilm" HeaderText="Id Film" ReadOnly="True" SortExpression="idfilm" />
                        <asp:BoundField DataField="idcategorie" HeaderText="Id Categorie" ReadOnly="True" SortExpression="idcategorie" />
                        <asp:BoundField DataField="datepub" HeaderText="Date Pub" ReadOnly="True" SortExpression="datepub"  />
                        <asp:CheckBoxField DataField="cinemaproj" HeaderText="Cinema Proj" SortExpression="cinemaproj" />
                        <asp:CommandField ShowInsertButton="True" />
                    </Fields>
                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                </asp:DetailsView>

                
                

            </div>
        </div>
       
    </div>
    </form>
</asp:Content>
