﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SelectionnerFilm.aspx.cs" Inherits="_420_PS1_lab2.SelectionnerFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    <title>SelectionnerFilm</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderImage" runat="server">
    <img class="mr-3 imgSize " src="../img/logoTeccart.jpeg" alt="Image Teccart">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderInfo" runat="server">
    
    <form id="form1" runat="server">
    
    <blockquote class="blockquote text-center">
        <p class="mb-0">Selection des film dans la base de donnee</p>
        <footer class="blockquote-footer">Avec des Select </footer>
    </blockquote>
        
        <div class="row main">
            <div class="col-6 ">
                
                <blockquote class="blockquote text-center">
                    <p class="mb-0">Affichage des film dans la base de donnee</p>                   
                </blockquote>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [film]"></asp:SqlDataSource>
    

                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="titre,nomdirecteur,photo" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" PageSize="4">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="titre" HeaderText="Titre" SortExpression="titre" />
                        <asp:BoundField DataField="nomdirecteur" HeaderText="Nom Directeur" SortExpression="nomdirecteur" />
                        <asp:ImageField DataImageUrlField="photo" DataImageUrlFormatString="~/img/{0}" HeaderText="Photo">
                            <ControlStyle Height="50px" Width="50px" />
                        </asp:ImageField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                

            </div>
    
            <div class="col-6 ">
                <blockquote class="blockquote text-center">
                    <p class="mb-0">Affichage des selections film</p>                   
                </blockquote>
 
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [titre], [nomdirecteur], [photo] FROM [film] WHERE ([titre] = @titre)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GridView1" Name="titre" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource2" PageSize="3">
                    <Columns>
                        <asp:BoundField DataField="titre" HeaderText="titre" SortExpression="titre" />
                        <asp:BoundField DataField="nomdirecteur" HeaderText="nomdirecteur" SortExpression="nomdirecteur" />
                        <asp:ImageField DataImageUrlField="photo" DataImageUrlFormatString="~/img/{0}" HeaderText="PHOTO">
                        </asp:ImageField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
                
                

            </div>
        </div>
    
    
    </form>
    

</asp:Content>
