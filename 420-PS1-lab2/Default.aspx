﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_420_PS1_lab2.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    <title>Default Page</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderImage" runat="server">
    <img class="mr-3 imgSize " src="../img/logoTeccart.jpeg" alt="Image Teccart">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderInfo"  runat="server">
    <form id="form1" runat="server">
    <div class="defaultInfo spacedef">
    <blockquote class="blockquote text-center">
        <p class="mb-0">This web is for manupilating Data from SQL using DataGridview</p>
        <footer class="blockquote-footer">Creation de la base de donnee <cite title="Films">Source Title</cite></footer>
    </blockquote>
    <br/>
    <br/>
    
    
        <dl class="row">
            <dt class="col-sm-3">Description Database</dt>
            <dd class="col-sm-9">La base de données FILMS.</dd>

            <dt class="col-sm-3">Tables</dt>
            <dd class="col-sm-9">
                <p>une table contenant film avec les champs .</p>
                <dl class="row">
                    <dt class="col-sm-4">Champs de la table lesfilms</dt>
                    <dd class="col-sm-8">titre, directeur, date de publication,hyperlien vers le site du film, image représentant le film, indication si le film est en salle ou non un nombre entier faisant référence à la catégorie du film.</dd>
                </dl>
                <p>une autre table contenant les categories avec les champs .</p>
                <dl class="row">
                    <dt class="col-sm-4">Champs de la table categorie</dt>
                    <dd class="col-sm-8">le nom de la catégorie.</dd>
                </dl> 
            </dd>
        </dl>
        
        
    </div>
</form>
</asp:Content>