﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AfficherFilm.aspx.cs" Inherits="_420_PS1_lab2.AfficherFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    <title>AfficherFilm</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderImage" runat="server">
    <img class="mr-3 imgSize " src="../img/logoTeccart.jpeg" alt="Image Teccart">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderInfo" runat="server">
   
    <blockquote class="blockquote text-center">
        <p class="mb-0">Affichage des films, redirection liens et photos</p>
        <footer class="blockquote-footer"><asp:Label ID="Label1" runat="server"  Text=""></asp:Label></footer>
    </blockquote>
      <form id="form1" runat="server">
    
      <div class="afficherInfo spacedef">
         
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT categorie.nomcategorie, film.titre, film.liens, film.photo FROM film INNER JOIN cinema ON film.idfilm = cinema.idfilm INNER JOIN categorie ON cinema.idcategorie = categorie.idcategorie"></asp:SqlDataSource>
          <asp:GridView ID="GridView1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True" OnPreRender="GridView1_PreRender" PageSize="4" AutoGenerateEditButton="True"  >
              <Columns>
                  <asp:TemplateField HeaderText="Categorie" SortExpression="nomcategorie">
                      <ItemTemplate>
                          <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="nomcategorie" DataValueField="nomcategorie" SelectedValue='<%# Bind("nomcategorie") %>'>
                          </asp:DropDownList>
                      </ItemTemplate>
                      <ControlStyle Width="150px" />
                      <ItemStyle Width="100px" />
                  </asp:TemplateField>
                  
                  <asp:HyperLinkField DataNavigateUrlFields="liens" DataTextField="titre" HeaderText="TITRE" >
                  <ControlStyle Width="300px" />
                  </asp:HyperLinkField>
                  <asp:ImageField DataImageUrlField="photo" DataImageUrlFormatString="~/img/{0}" HeaderText="PHOTO">
                      <ControlStyle BorderStyle="Solid" BorderWidth="1px" Height="50px" Width="50px" />
                  </asp:ImageField>
                  
              </Columns>

              <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510"></FooterStyle>

              <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White"></HeaderStyle>

              <PagerStyle HorizontalAlign="Center" ForeColor="#8C4510"></PagerStyle>

              <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510"></RowStyle>

              <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

              <SortedAscendingCellStyle BackColor="#FFF1D4"></SortedAscendingCellStyle>

              <SortedAscendingHeaderStyle BackColor="#B95C30"></SortedAscendingHeaderStyle>

              <SortedDescendingCellStyle BackColor="#F1E5CE"></SortedDescendingCellStyle>

              <SortedDescendingHeaderStyle BackColor="#93451F"></SortedDescendingHeaderStyle>
              
          </asp:GridView>
         

      </div>
          <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [categorie]" DeleteCommand="DELETE FROM [categorie] WHERE [idcategorie] = @idcategorie" InsertCommand="INSERT INTO [categorie] ([idcategorie], [nomcategorie]) VALUES (@idcategorie, @nomcategorie)" UpdateCommand="UPDATE [film] SET [idcategorie] = (select idcategorie from categorie where nomcategorie = @nomcategorie) ">
              <DeleteParameters>
                  <asp:Parameter Name="idcategorie" Type="Int32" />
              </DeleteParameters>
              <InsertParameters>
                  <asp:Parameter Name="idcategorie" Type="Int32" />
                  <asp:Parameter Name="nomcategorie" Type="String" />
              </InsertParameters>
              <UpdateParameters>
                  <asp:Parameter Name="nomcategorie" Type="String" />
                  <asp:Parameter Name="idcategorie" Type="Int32" />
              </UpdateParameters>
          </asp:SqlDataSource>
     
      </form>
      
     
</asp:Content>
